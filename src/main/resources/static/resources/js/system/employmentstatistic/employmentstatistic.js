layui.use(['element', 'table'], function(){
    var table = layui.table,
        $ = layui.$;

    //下面四个render用来渲染四个表格

    table.render({
        elem : '#majorStaList',
        page : true,//开启分页
        url : '/employment/statistic/student_major',
        cellMinWidth : 95,
        height : '400px',
        limit : 3,//每页条数
        limits : [3, 6, 9, 12],//可切换的每页条数
        autoSort : false,
        id : 'majorStaList',
        method: "post",
        cols : [[
            {field:'student_major', title:'学生专业', align:'center'},
            {field:'studentCount', title:'就业人数', align:'center'},
            {title:'操作', align:'center', fixed:'right', toolbar:'#statisticListOpt'}
        ]]
    });

    table.render({
        elem : '#companyStaList',
        page : true,//开启分页
        url : '/employment/statistic/company_name',
        cellMinWidth : 95,
        height : '400px',
        limit : 3,//每页条数
        limits : [3, 6, 9, 12],//可切换的每页条数
        autoSort : false,
        id : 'companyStaList',
        method: "post",
        cols : [[
            {field:'company_name', title:'就业单位', align:'center'},
            {field:'studentCount', title:'就业人数', align:'center'},
            {title:'操作', align:'center', fixed:'right', toolbar:'#statisticListOpt'}
        ]]
    });

    table.render({
        elem : '#classStaList',
        page : true,//开启分页
        url : '/employment/statistic/student_class',
        cellMinWidth : 95,
        height : '400px',
        limit : 3,//每页条数
        limits : [3, 6, 9, 12],//可切换的每页条数
        autoSort : false,
        id : 'classStaList',
        method: "post",
        cols : [[
            {field:'student_class', title:'学生班级', align:'center'},
            {field:'studentCount', title:'就业人数', align:'center'},
            {title:'操作', align:'center', fixed:'right', toolbar:'#statisticListOpt'}
        ]]
    });

    table.render({
        elem : '#stationStaList',
        page : true,//开启分页
        url : '/employment/statistic/employment_station',
        cellMinWidth : 95,
        height : '400px',
        limit : 3,//每页条数
        limits : [3, 6, 9, 12],//可切换的每页条数
        autoSort : false,
        id : 'stationStaList',
        method: "post",
        cols : [[
            {field:'employment_station', title:'就业岗位', align:'center'},
            {field:'studentCount', title:'就业人数', align:'center'},
            {title:'操作', align:'center', fixed:'right', toolbar:'#statisticListOpt'}
        ]]
    });

    //下面四个on用来为四个表格绑定事件

    table.on("tool(majorStaList)",function (obj) {
        //obj即为当前行对象
        table.render({
            elem : '#table-detail-hidden',
            page : true,//开启分页
            url : '/employment/getallinfo?studentMajor='+obj.data.student_major,
            cellMinWidth : 95,
            height : '400px',
            limit : 20,//每页条数
            limits : [10, 15, 20, 25],//可切换的每页条数
            autoSort : false,
            // id : 'majorStaList',
            method: "post",
            cols : [[
                {field:'companyName', title:'就业单位', align:'center'},
                {field:'companyAddress', title:'就业单位地址', align:'center'},
                {field:'employmentStation', title:'就业岗位', align:'center'},
                {field:'treatment', title:'待遇', align:'center'},
                {field:'studentName', title:'学生姓名', align:'center'},
                {field:'studentMajor', title:'学生专业', align:'center'},
                {field:'studentGender', title:'学生性别', align:'center'},
                {field:'studentClass', title:'学生班级', align:'center'},
                {field:'studentMobile', title:'学生电话', align:'center'},
                {field:'employmentTime', title:'就业时间', align:'center'},
            ]]
        });
        layer.open({
            type: 1,
            title:"详细情况",
            area: ["1000px","500px"],
            content:$("#box-table-detail-hidden")
        })
    });
    table.on("tool(classStaList)",function (obj) {
        table.render({
            elem : '#table-detail-hidden',
            page : true,//开启分页
            url : '/employment/getallinfo?studentClass='+obj.data.student_class,
            cellMinWidth : 95,
            height : '400px',
            limit : 20,//每页条数
            limits : [10, 15, 20, 25],//可切换的每页条数
            autoSort : false,
            // id : 'majorStaList',
            method: "post",
            cols : [[
                {field:'companyName', title:'就业单位', align:'center'},
                {field:'companyAddress', title:'就业单位地址', align:'center'},
                {field:'employmentStation', title:'就业岗位', align:'center'},
                {field:'treatment', title:'待遇', align:'center'},
                {field:'studentName', title:'学生姓名', align:'center'},
                {field:'studentMajor', title:'学生专业', align:'center'},
                {field:'studentGender', title:'学生性别', align:'center'},
                {field:'studentClass', title:'学生班级', align:'center'},
                {field:'studentMobile', title:'学生电话', align:'center'},
                {field:'employmentTime', title:'就业时间', align:'center'},
            ]]
        });
        layer.open({
            type: 1,
            title:"详细情况",
            area: ["1000px","500px"],
            content:$("#box-table-detail-hidden")
        })
    });
    table.on("tool(stationStaList)",function (obj) {
        table.render({
            elem : '#table-detail-hidden',
            page : true,//开启分页
            url : '/employment/getallinfo?employmentStation='+obj.data.employment_station,
            cellMinWidth : 95,
            height : '400px',
            limit : 20,//每页条数
            limits : [10, 15, 20, 25],//可切换的每页条数
            autoSort : false,
            // id : 'majorStaList',
            method: "post",
            cols : [[
                {field:'companyName', title:'就业单位', align:'center'},
                {field:'companyAddress', title:'就业单位地址', align:'center'},
                {field:'employmentStation', title:'就业岗位', align:'center'},
                {field:'treatment', title:'待遇', align:'center'},
                {field:'studentName', title:'学生姓名', align:'center'},
                {field:'studentMajor', title:'学生专业', align:'center'},
                {field:'studentGender', title:'学生性别', align:'center'},
                {field:'studentClass', title:'学生班级', align:'center'},
                {field:'studentMobile', title:'学生电话', align:'center'},
                {field:'employmentTime', title:'就业时间', align:'center'},
            ]]
        });
        layer.open({
            type: 1,
            title:"详细情况",
            area: ["1000px","500px"],
            content:$("#box-table-detail-hidden")
        })
    });
    table.on("tool(companyStaList)",function (obj) {
        table.render({
            elem : '#table-detail-hidden',
            page : true,//开启分页
            url : '/employment/getallinfo?companyName='+obj.data.company_name,
            cellMinWidth : 95,
            height : '400px',
            limit : 20,//每页条数
            limits : [10, 15, 20, 25],//可切换的每页条数
            autoSort : false,
            // id : 'majorStaList',
            method: "post",
            cols : [[
                {field:'companyName', title:'就业单位', align:'center'},
                {field:'companyAddress', title:'就业单位地址', align:'center'},
                {field:'employmentStation', title:'就业岗位', align:'center'},
                {field:'treatment', title:'待遇', align:'center'},
                {field:'studentName', title:'学生姓名', align:'center'},
                {field:'studentMajor', title:'学生专业', align:'center'},
                {field:'studentGender', title:'学生性别', align:'center'},
                {field:'studentClass', title:'学生班级', align:'center'},
                {field:'studentMobile', title:'学生电话', align:'center'},
                {field:'employmentTime', title:'就业时间', align:'center'},
            ]]
        });
        layer.open({
            type: 1,
            title:"详细情况",
            area: ["1000px","500px"],
            content:$("#box-table-detail-hidden")
        })
    });
});



// import * as echarts from "./echarts"




//生成echarts统计图
function generateCharts(title,method,domid){
    //title:表格标题;method: 统计方式;dom:挂载点domid
    $.ajax({
        type: "post",
        url: "/employment/statistic/" + method,
        data: {
            "limit":20,
            "page": 1,
        },
        success: function (data) {
            if (data.code == 0){
                let xAxis = [],yAxis = [];
                for (var i = 0;i < data.data.length;i++){
                    xAxis.push(data.data[i][method]);
                    yAxis.push(data.data[i]["studentCount"]);
                }
                let mychart = echarts.init(document.getElementById(domid));

                let option = {
                    title:{
                        text:title,
                        x:"center" //标题的水平位置
                        // textAlign:"center"  //标题的水平对齐方式,
                    },
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis: [
                        {
                            type:"category",
                            data:xAxis,
                            axisTick:{
                                alignWithLabel: true
                            }
                        }
                    ],
                    yAxis:[
                        {
                            type: "value",
                        }
                    ],
                    series: [
                        {
                            name: "studentCount",
                            type: "bar",
                            barWidth: "60%",
                            data: yAxis
                        }
                    ]

                };
                mychart.setOption(option);
            }
        }
    })
}


window.onload =function(){
    generateCharts("按专业统计","student_major","chartBox_major");
    generateCharts("按企业统计","company_name","chartBox_company");
    generateCharts("按班级统计","student_class","chartBox_class");
    generateCharts("按岗位统计","employment_station","chartBox_station");
};
