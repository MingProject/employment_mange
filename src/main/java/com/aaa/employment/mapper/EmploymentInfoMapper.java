package com.aaa.employment.mapper;

import com.aaa.employment.mapper.entity.EmploymentInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

@Repository
public interface EmploymentInfoMapper {
    List<EmploymentInfo> getAllEmploymentInfo(EmploymentInfo employmentInfo);
    List<EmploymentInfo> getEmploymentInfo(EmploymentInfo employmentInfo);
    List<Map<String, String>> getStudentCount(String fieldName);
    int addEmploymentInfo(EmploymentInfo employmentInfo);
    int updateEmploymentInfo(EmploymentInfo employmentInfo);
    int deleteEmploymentInfo(String infoId);
    int deleteEmploymentInfoSelect(List<String> list);
    //获取专业、班级、岗位、企业种类个数
    int getFieldCount(@Param("fieldName") String fieldName);
}
