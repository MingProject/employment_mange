package com.aaa.employment.util;

import com.aaa.employment.mapper.entity.User;
import com.aaa.employment.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import javax.annotation.Resource;

public class MyRealm extends AuthorizingRealm{
    @Resource
    private UserService userService;
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("授权开始");

        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("认证开始");
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;

        String userAccount = usernamePasswordToken.getUsername();
        User user = userService.getUserByAccount(userAccount);
        if(user == null){
            return null;
        }
        ByteSource credentialsSalt = ByteSource.Util.bytes(user.getUserSalt());
        /**
         * Object principal  身份, Object credentials   密码, String realmName  realm 名曾
         * 密码对比器
         */
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(
                user,
                user.getUserPwd(),
                credentialsSalt,
                getName());

        return simpleAuthenticationInfo;
    }
}
