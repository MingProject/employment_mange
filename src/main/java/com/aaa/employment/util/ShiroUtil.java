package com.aaa.employment.util;

import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.SimpleHash;

public class ShiroUtil {
    //shiro 的 加盐加密
    public static String getShiroPassword(String mpassword,String salt){
        //加密算法：String algorithmName,
        //明文： Object source,
        //加盐： Object salt,
        //加密次数： int hashIterations
//        String algorithmName = "md5";
//        Object source = (Object)mpassword;
//        Object addsalt = (Object)salt;
//        int hashIterations = 1000;
        SimpleHash simpleHash = new SimpleHash(FixUtil.algorithmName, mpassword,salt,FixUtil.hashIterations);
        return simpleHash.toString();
    }

    //凭证匹配器对象
    public static CredentialsMatcher credentialsMatcher(){
        HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
        credentialsMatcher.setHashAlgorithmName(FixUtil.algorithmName);
        credentialsMatcher.setHashIterations(FixUtil.hashIterations);
        return credentialsMatcher;
    }
}
