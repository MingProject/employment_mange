package com.aaa.employment.util;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {
    @Bean
    public MyRealm ReturnMyRealm(){
        MyRealm myRealm = new MyRealm();
        myRealm.setCredentialsMatcher(ShiroUtil.credentialsMatcher());
        return myRealm;
    }
    @Bean
    public DefaultWebSecurityManager defaultWebSecurityManager(){
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(ReturnMyRealm());
        return defaultWebSecurityManager;
    }
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(){
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(defaultWebSecurityManager());
        Map<String,String> map = new LinkedHashMap();
        /**
         * 过滤请求
         * anon:无需认证，放行
         * anonc:所有请求必须登录
         * logout ： 推出过滤器
         * user ：登陆过的过滤器
         *
         * 权限分类：
         * perms：：权限过滤器
         * roles：校色权限过滤器
         */
        map.put("/employment/login","anon"); //对谁放行
//        map.put("/resources/plugins/layui/layui.js","anon"); //对谁放行
//        map.put("/resources/js/system/home/login.js","anon"); //对谁放行
        map.put("/resources/**","anon"); //对谁放行
        map.put("/**","authc"); //对所有请求进行认证
        shiroFilterFactoryBean.setFilterChainDefinitionMap(map);
        shiroFilterFactoryBean.setLoginUrl("/");

        return shiroFilterFactoryBean;
    }
}
