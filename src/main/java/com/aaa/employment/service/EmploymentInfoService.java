package com.aaa.employment.service;

import com.aaa.employment.mapper.entity.EmploymentInfo;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EmploymentInfoService {
    PageInfo<EmploymentInfo> getAllEmploymentInfo(EmploymentInfo employmentInfo, int pageNum, int pageSize);
    List<EmploymentInfo> getEmploymentInfo(EmploymentInfo employmentInfo, int pageNum, int pageSize);
    List<Map<String, String>> getStudentCount(String fieldName, int pageNum, int pageSize);
    void addEmploymentInfo(EmploymentInfo employmentInfo);
    void updateEmploymentInfo(EmploymentInfo employmentInfo);
    void deleteEmploymentInfo(String infoId);
    void deleteEmploymentInfoSelect(String idList);
    //获取专业、班级、岗位、企业种类个数
    int getFieldCount(@Param("fieldName") String fieldName);
}
