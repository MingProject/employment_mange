package com.aaa.employment.service.impl;

import com.aaa.employment.common.CommonResult;
import com.aaa.employment.util.ShiroUtil;
import com.github.pagehelper.PageHelper;
import com.aaa.employment.mapper.UserMapper;
import com.aaa.employment.mapper.entity.User;
import com.aaa.employment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public List<User> getAllUsers(User user, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return userMapper.getAllUsers(user);
    }

    @Override
    public User getUserById(String userId) {
        return userMapper.getUserById(userId);
    }

    @Override
    public User getUserByAccount(String userAccount) {
        return userMapper.getUserByAccount(userAccount);
    }

    @Override
    public CommonResult<Integer> addUser(User user) {
        User userByAccount = userMapper.getUserByAccount(user.getUserAccount());

        if (userByAccount == null){
            user.setUserId(UUID.randomUUID().toString());
            String salt = UUID.randomUUID().toString();
            // 对密码进行加密处理
            user.setUserPwd(ShiroUtil.getShiroPassword(user.getUserPwd(), salt));
            user.setUserSalt(salt);
            userMapper.addUser(user);
            return CommonResult.generateSuccessResult(1, 1);
        }else {
            return CommonResult.generateFailureResult(0, 0);
        }
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    @Override
    public void deleteUser(String userId) {
        userMapper.deleteUser(userId);
    }

    @Override
    public int getUserCount() {
        return userMapper.getUserCount();
    }
}
