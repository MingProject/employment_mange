package com.aaa.employment.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.aaa.employment.mapper.EmploymentInfoMapper;
import com.aaa.employment.mapper.entity.EmploymentInfo;
import com.aaa.employment.service.EmploymentInfoService;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EmploymentInfoServiceImpl implements EmploymentInfoService{
    @Autowired
    EmploymentInfoMapper employmentInfoMapper;


    @Override
    public PageInfo<EmploymentInfo> getAllEmploymentInfo(EmploymentInfo employmentInfo, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<EmploymentInfo> allEmploymentInfo = employmentInfoMapper.getAllEmploymentInfo(employmentInfo);
        PageInfo<EmploymentInfo> pageInfo = new PageInfo<>(allEmploymentInfo);

        return pageInfo;
    }

    @Override
    public List<EmploymentInfo> getEmploymentInfo(EmploymentInfo employmentInfo, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return employmentInfoMapper.getEmploymentInfo(employmentInfo);
    }

    @Override
    public List<Map<String, String>> getStudentCount(String fieldName, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return employmentInfoMapper.getStudentCount(fieldName);
    }
    @Override
    //获取专业、班级、岗位、企业种类个数
    public int getFieldCount(@Param("fieldName") String fieldName){
        return employmentInfoMapper.getFieldCount(fieldName);
    }

    @Override
    public void addEmploymentInfo(EmploymentInfo employmentInfo) {
        employmentInfoMapper.addEmploymentInfo(employmentInfo);
    }

    @Override
    public void updateEmploymentInfo(EmploymentInfo employmentInfo) {
        employmentInfoMapper.updateEmploymentInfo(employmentInfo);
    }

    @Override
    public void deleteEmploymentInfo(String infoId) {
        employmentInfoMapper.deleteEmploymentInfo(infoId);
    }

    @Override
    public void deleteEmploymentInfoSelect(String idList) {
        List<String> list = (List<String>) JSON.parse(idList);
        employmentInfoMapper.deleteEmploymentInfoSelect(list);
    }
}
