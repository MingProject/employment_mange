package com.aaa.employment.service;

import com.aaa.employment.common.CommonResult;
import com.aaa.employment.mapper.entity.User;
import java.util.List;

public interface UserService {
    List<User> getAllUsers(User user, int pageNum, int pageSize);
    User getUserById(String userId);
    User getUserByAccount(String userAccount);
    CommonResult<Integer> addUser(User user);
    void updateUser(User user);
    void deleteUser(String userId);
    int getUserCount();
}
