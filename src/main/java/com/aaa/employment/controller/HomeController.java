package com.aaa.employment.controller;

import com.aaa.employment.common.CommonResult;
import com.aaa.employment.mapper.entity.User;
import com.aaa.employment.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {
    @Autowired
    UserService userService;

    @RequestMapping({"/","/employment"})
    public String index(){
        return "system/login";
    }

    @ResponseBody
    @RequestMapping("/employment/login")
    public CommonResult<User> login(User user){
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(user.getUserAccount(),user.getUserPwd());
        try {
            subject.login(usernamePasswordToken);
        }catch (UnknownAccountException e){
            return CommonResult.generateFailureResult("帐号不正确", 1, null);
        }catch (IncorrectCredentialsException e){
            return  CommonResult.generateFailureResult("密码不正确", 1, null);
        }

        return CommonResult.generateSuccessResult(1, userService.getUserByAccount(user.getUserAccount()));
    }
}
