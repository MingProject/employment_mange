package com.aaa.employment.controller;

import com.aaa.employment.mapper.entity.User;
import com.aaa.employment.service.UserService;
import com.aaa.employment.common.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping("/employment/usermanage")
    public String index(){
        return "system/usermanage/usermanage";
    }

    @ResponseBody
    @RequestMapping("/employment/getallusers")
    public CommonResult<List<User>> getAllUsers(User user, @RequestParam("limit") int pageSize, @RequestParam("page") int pageNum){
        List<User> result = userService.getAllUsers(user, pageNum, pageSize);
        return CommonResult.generateSuccessResult(userService.getUserCount(), result);
    }

    @ResponseBody
    @RequestMapping("/employment/getuserbyaccount/{userAccount}")
    public CommonResult<User> getUserByAccount(@PathVariable("userAccount") String userAccount){
        return CommonResult.generateSuccessResult(1, userService.getUserByAccount(userAccount));
    }

    @ResponseBody
    @RequestMapping("/employment/adduser")
    public CommonResult<Integer> addUser(User user){
        return  userService.addUser(user);
    }

    @ResponseBody
    @RequestMapping("/employment/updateuser")
    public CommonResult<Integer> updateUser(User user){
        userService.updateUser(user);
        return CommonResult.generateSuccessResult(1, 1);
    }

    @ResponseBody
    @RequestMapping("/employment/deluser/{userId}")
    public CommonResult<Integer> delInfo(@PathVariable("userId") String userId){
        userService.deleteUser(userId);
        return CommonResult.generateSuccessResult(1, 1);
    }
}
