package com.aaa.employment.controller;

import com.aaa.employment.common.CommonResult;
import com.aaa.employment.mapper.entity.EmploymentInfo;
import com.aaa.employment.service.EmploymentInfoService;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.UUID;

@Controller
public class EmploymentInfoController {
    @Autowired
    EmploymentInfoService employmentInfoService;

    @RequestMapping({"/employment/index", "/employment/employmentinfo"})
    public String index(){
        return "system/employmentinfo/employmentinfo";
    }

    @ResponseBody
    @RequestMapping("/employment/getallinfo")
    public CommonResult<List<EmploymentInfo>> getAllInfo(EmploymentInfo employmentInfo, @RequestParam("limit") int pageSize, @RequestParam("page") int pageNum){

        PageInfo<EmploymentInfo> pageInfo = employmentInfoService.getAllEmploymentInfo(employmentInfo, pageNum, pageSize);

        CommonResult<List<EmploymentInfo>> rtInfoResult = CommonResult.generateSuccessResult(pageInfo.getTotal(), pageInfo.getList());

        return rtInfoResult;
    }

    @ResponseBody
    @RequestMapping("/employment/getinfo")
    public CommonResult<List<EmploymentInfo>> getinfo(EmploymentInfo info, @RequestParam("limit") int pageSize, @RequestParam("page") int pageNum){
        List<EmploymentInfo> infoList = employmentInfoService.getEmploymentInfo(info, pageNum, pageSize);
        CommonResult<List<EmploymentInfo>> rtInfoResult = CommonResult.generateSuccessResult(infoList.size(), infoList);

        return rtInfoResult;
    }

    @ResponseBody
    @RequestMapping("/employment/addinfo")
    public CommonResult<Integer> addInfo(EmploymentInfo info){
        info.setInformationId(UUID.randomUUID().toString());
        employmentInfoService.addEmploymentInfo(info);
        return CommonResult.generateSuccessResult(1, 1);
    }

    @ResponseBody
    @RequestMapping("/employment/updateinfo")
    public CommonResult<Integer> updateInfo(EmploymentInfo info){
        employmentInfoService.updateEmploymentInfo(info);
        return CommonResult.generateSuccessResult(1, 1);
    }

    @ResponseBody
    @RequestMapping("/employment/delinfo/{infoId}")
    public CommonResult<Integer> delInfo(@PathVariable("infoId") String infoId){
        employmentInfoService.deleteEmploymentInfo(infoId);
        return CommonResult.generateSuccessResult(1, 1);
    }
    @ResponseBody
    @RequestMapping("/employment/delInfoSelect/")
    public CommonResult<Integer> delInfoSelect(String idlist){
        employmentInfoService.deleteEmploymentInfoSelect(idlist);
        return CommonResult.generateSuccessResult(1, 1);
    }

    @RequestMapping("/employment/logout")
    public String logout(){
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "redirect:/employment";
    }
}
