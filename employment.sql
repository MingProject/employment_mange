-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: employment
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employment_info`
--

DROP TABLE IF EXISTS `employment_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employment_info` (
  `information_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `company_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `employment_station` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `treatment` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ability_requirement` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `student_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `student_major` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `student_class` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `student_mobile` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `employment_time` date DEFAULT NULL,
  `company_contact_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `company_contact_mobile` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `student_gender` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`information_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employment_info`
--

LOCK TABLES `employment_info` WRITE;
/*!40000 ALTER TABLE `employment_info` DISABLE KEYS */;
INSERT INTO `employment_info` VALUES ('591dc94a-3783-4a86-8770-959b437ba831','腾讯','深圳','后端开发','30k','spring全家桶','恒威','软件工程','互联网182','15138116119','2021-12-15','秦国庆','15138116119','男'),('7d389e90-ac94-4a34-a773-b559cd016fe6','快手','北京','前端','20k','前端框架','张三','人工智能','人工智能182','15138116119','2021-12-17','秦国庆','15138116119','男'),('918d04af-11ca-4b17-a77e-f372bb00321e','华为','杭州','java开发','40*20','精通java语言','周七','计算机科学与技术','16级','15188888888','2020-05-30','逍遥子','15100000000','男'),('c5720736-8d6e-4053-b13e-1bb1c6fd7646','字节条动','上海','后端开发','20k','掌握spring全家桶','恒威','计算机科学与技术','互联网182','123124155','2021-12-09','秦国庆','15138116119','男'),('d7752b77-fd6f-4fe7-ad71-817b3277b9fe','dda\'s\'f\'sa\'f','asfsafasfa\'s\'fa\'s\'fa\'s\'fa','asfasfasfasfa\'fa\'fa','fafafafa\'s\'fa\'s\'fa','fa\'s\'fa\'fa','永强','sadasd','dasdas','15138116119','2021-12-15','dasdfasa','15138116119','男');
/*!40000 ALTER TABLE `employment_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_account` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_salt` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_pwd` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL COMMENT '0-管理员，1-普通用户',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('001001001001','test','测试',NULL,'e10adc3949ba59abbe56e057f20f883e',1),('1234567890','admin','管理员',NULL,'e10adc3949ba59abbe56e057f20f883e',0),('edd76ea9-90bf-4fb2-a3c6-737e083be6f5','moon','小明','339c5770-78b5-4e69-b322-384550247a65','c23ea75aeadb0476b998b666796d82e6',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-10 15:47:09
